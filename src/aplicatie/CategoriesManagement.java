package aplicatie;

import java.util.ArrayList;
import java.util.List;

public class CategoriesManagement {
	static List<Category> categories = new ArrayList<Category>();
	public static void categoriesMenu() {
		System.out.println("CATEGORY MANAGEMENT");
		System.out.println("1.Add category");
		System.out.println("2.Remove category");
		System.out.println("3.Back to main menu");
		String option = Menu.getOption();
		
		switch(option) {
		case "1":
			addCategory();
			break;
		case "2":
			removeCategory();
			break;
		case "3":
			Menu.displayMenu();
			break;
		default:
			System.out.println("Please select an option between 1 - 3!");
			categoriesMenu();
		}
	}
	
	public static void addCategory() {
		System.out.println("Please enter a name of the category you want to add:");
		String name = Menu.getOption();
		Category c = new Category(name);
		categories.add(c);
		categoriesMenu();
	}
	
	public static void removeCategory() {
		System.out.println("Please enter the name of the category you want to delete:");
		String name = Menu.getOption();
		categories.removeIf(categoryName -> categoryName.getName().equals(name));
		categoriesMenu();
	}
}
