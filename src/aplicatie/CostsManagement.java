package aplicatie;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CostsManagement {
	static List<Cost> costs = new ArrayList<Cost>();
	public static void costsMenu() {
		System.out.println("Please select one of the options below:");
		System.out.println("1. Add cost");
		System.out.println("2. Remove cost");
		System.out.println("3. View");
		System.out.println("4. Back to main menu");
		String option = Menu.getOption();

		switch(option) {
		case "1":
			addCost();
			break;
		case "2":
			removeCost();
			break;
		case "3":
			viewBy();
			break;
		case "4":
			Menu.displayMenu();
		}
	}

	public static void addCost() {
		int found = 0;
		System.out.println("Please enter the month:");
		String month = Menu.getOption();
		System.out.println("Please enter the day and year in format '31-2020'");
		String date = Menu.getOption();
		System.out.println("Please enter the amound of cost:");
		int amount = Integer.parseInt(Menu.getOption());
		System.out.println("Please enter the description of cost:");
		String description = Menu.getOption();
		System.out.println("Please enter the details of cost:");
		String details = Menu.getOption();
		System.out.println("Please enter the category of cost:");
		System.out.println("Categories available" + CategoriesManagement.categories);
		String category = Menu.getOption();
		for(Category categorie : CategoriesManagement.categories) {
			if(categorie.getName().equals(category)) {
				found = 1;
			} 
		}
		if(found == 1) {
			Cost cost = new Cost(month, date, category, amount, description, details);
			costs.add(cost);
		} else {
			System.out.println("The specified category does not exist");
			System.out.println("Returning to costs menu...");

		}
		costsMenu();
	}

	public static void removeCost() {
	
		if(costs.isEmpty()) {
			System.out.println("There are no costs to remove");
			costsMenu();
		} else {
			System.out.println("Please enter the month of the cost that you want to delete:");
			String month = Menu.getOption();
			costs.forEach(costt -> {if(costt.getMonth().equals(month)) {System.out.println(costs.indexOf(costt) +"."+ costt);}});
			
			//costs.stream().filter(costt -> costt.getMonth().equals(month)).forEach(costt -> System.out.println(costs.indexOf(costt) +"."+ costt));
		} 
		System.out.println("Please enter the number of cost that you want to delete:");
		String selectedCost = Menu.getOption();
		costs.remove(Integer.parseInt(selectedCost));
		costsMenu();
	}
	public static void viewBy() {
		System.out.println("View costs by:");
		System.out.println("1.Month");
		System.out.println("2.Month and category");
		System.out.println("3.Compared to the previous month");
		System.out.println("4.Back to costs menu");
		String option = Menu.getOption();

		switch(option) {
		case "1":
			viewCostsByMonth();
			break;
		case "2":
			viewCostsByMonthAndCategory();
			break;
		case "3":
			viewCostsComparedToLastMonth();
			break;
		case "4":
			costsMenu();
			break;
		default:
			System.out.println("Please select an option between 1 - 4");
		}
	}
	public static void viewCostsByMonth() {
		System.out.println("Please enter the month of costs that you want to see");
		String month = Menu.getOption();
		try {
			if(Menu.isNumeric(month) == false) {
				throw new ArithmeticException ("The entered month isn't numeric, please enter a month between 01-12");
			}
		} catch (ArithmeticException e) {
			System.out.println(e.getLocalizedMessage());
			viewBy();
		}
		costs.forEach(costt -> {if(costt.getMonth().equals(month)) {System.out.println(costs.indexOf(costt)+ "." + costt);}});
		viewBy();
	}
	public static void viewCostsByMonthAndCategory() {
		List<Cost> selectedCosts = new ArrayList<Cost>();
		//selectedCosts.removeAll(selectedCosts);
		System.out.println("Enter the category of costs that you want to see:");
		String category = Menu.getOption();
		System.out.println("Enter the month of costs that you want to see:");
		String month = Menu.getOption();
		try {
			if(Menu.isNumeric(month) == false) {
				throw new ArithmeticException ("The entered month isn't numeric, please enter a month between 01-12");
			}
		} catch (ArithmeticException e) {
			System.out.println(e.getLocalizedMessage());
			viewBy();
		}
		for(Cost cost : costs) {
			if(cost.getCategory().equals(category) && cost.getMonth().equals(month)) {
				selectedCosts.add(cost);
			}
		}
		System.out.println(selectedCosts);
		viewBy();
	}
	public static void viewCostsComparedToLastMonth() {
		int currentMonth = 0;
		int previousMonth = 0;
		System.out.println("Please enter the month that you want to compare with the previous month, from 1 - 12:");
		String month = Menu.getOption();
		
		try {
			if(Menu.isNumeric(month) == false) {
				throw new ArithmeticException ("The entered month isn't numeric, please enter a month between 01-12");
			}
		} catch (ArithmeticException e) {
			System.out.println(e.getLocalizedMessage());
			viewBy();
		}
		
		for(Cost cost : costs) {
			if(cost.getMonth().equals(month)) {
				currentMonth = currentMonth + cost.getAmount();
			}
		}
		for(Cost cost : costs) {
			if(cost.getMonth().equals(Integer.parseInt(month) - 1)) {
				previousMonth = previousMonth + cost.getAmount();
			}
		}
		if(currentMonth > previousMonth) {
			System.out.println("The costs of selected month are bigger with "+ (currentMonth - previousMonth) + " $");
		} else if (currentMonth < previousMonth) {
			System.out.println("The costs of selected month are lower with "+ (previousMonth - currentMonth) + " $");
		}
		viewBy();
	}
}
