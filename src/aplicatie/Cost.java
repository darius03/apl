package aplicatie;

public class Cost {
	private String date;
	private String category;
	private int amount;
	private String description;
	private String details;
	private String month;
	
	public Cost() {}
	
	public Cost(String month, String date, String category, int amount, String description, String details) {
		this.date = date;
		this.category = category;
		this.amount = amount;
		this.description = description;
		this.details = details;
		this.month=month;
		
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return "Cost [date=" + date + ", category=" + category + ", amount=" + amount + ", description=" + description
				+ ", details=" + details + ", month=" + month + "]";
	}

	
	
}
