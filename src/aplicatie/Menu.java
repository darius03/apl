package aplicatie;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		System.out.println("Select one of the options below:");
		displayMenu();
	}
	
	public static boolean isNumeric(String num) {
		if(num == null) {
			return false;
		}
		try {
			int i = Integer.parseInt(num);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	public static String getOption() {
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
	public static void displayMenu() {
		System.out.println("1. Categories management");
		System.out.println("2. Costs management");
		String option = getOption();
		
		switch(option) {
			case "1":
				CategoriesManagement.categoriesMenu();
				break;
			case "2":
				CostsManagement.costsMenu();
				break;
			default:
				System.out.println("Please select an option between 1 - 2 !");
				displayMenu();
		}
	}
}
